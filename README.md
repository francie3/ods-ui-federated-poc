# ODS Federation Module POC

Sample federated harness for hosting LPR

## Start

- ``npm run build``
- ``npm run start`
- Navigate to host at https://odsps.platform.navify.com:5000
- Navigate to standalone patients microfrontend at http://odsps.platform.navify.com:3000
- Verify url to where LPR is deployed along with patient to load in environment.ts
- For a different patient navigate to https://odsps.platform.navify.com:5000/patientid/newuuid



// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { domain } from './environment-common';

/*export const environment = {
  production: false,
  baseUrl: domain,
};*/

export const environment = {
  production: false,
  // lprUiUrl: 'https://dev-ods-patient-summary.dev.tb.platform.navify.com/remoteEntry.js',
  lprUiUrl:  'https://odsps.platform.navify.com:4200/remoteEntry.js',
  baseUrl: 'https://odsps.tbusdev-astack-tumorboard.dev.tb.platform.navify.com/tumor-board/api',
  defaultPatientId: '4a3edc34-2e56-4deb-811c-2d4afc337c61'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.

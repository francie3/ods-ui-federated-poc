const protocol = window.location.protocol || 'https:';
const hostname = window.location.hostname || 'localhost';
const port = window.location.port || null;
export const domain = port
  ? `${protocol}//${hostname}:${port}`
  : `${protocol}//${hostname}`;

import { Injectable } from '@angular/core';
import { Microfrontend } from './microfrontend';
import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class LookupService {
    lookup(): Promise<Microfrontend[]> {
        const patientidMatch = location.pathname.match(/\/(patientid|patient)\/(.*)/);
        return Promise.resolve([
            {
                // For Loading
                remoteEntry: 'https://odsps.platform.navify.com:3000/remoteEntry.js',
                remoteName: 'patients',
                exposedModule: './Module',

                // For Routing
                displayName: 'Patients',
                routePath: 'patients',
                ngModuleName: 'PatientsModule'
            },
            {
              // For Loading
              remoteEntry: environment.lprUiUrl,
              remoteName: 'patientsummary',
              exposedModule: './PatientFederatedModule',

              // For Routing
              displayName: 'Patient Summary',
              routePath: 'patient/:patientUuid',
              routeLink: `patient/${patientidMatch ? patientidMatch[2] : 'unknown'}`,
              ngModuleName: 'PatientSummaryFederatedModule'
            }
        ] as Microfrontend[]);
    }
}

import { Component, OnInit, OnDestroy, NgZone, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { buildRoutes } from '../menu-utils';
import { LookupService } from './microfrontends/lookup.service';
import { Microfrontend } from './microfrontends/microfrontend';
import { HostMessageBus, IMessage, PluginMessageBus } from '@dia/message-bus';
import {
  OdsApplication,
  ParentPostMessageType,
  ParentReceiveMessageType,
  PostMessageData,
  ReceiveMessageEvent,
} from './app.model';
import { HostMessageBus as OdsHostMessageBus } from '@ods/message-bus';
import { AppAuthService } from './auth/app-auth.service';
import { MessageBusAbstractComponent } from './abstract/message-bus-abstract-component';
import { ConfigService } from './config/config.service';
import { PatientService } from './patient/patient.service';

const messageCode = {
  patientSummaryLoaded: 'PatientSummaryLoaded',
  patientSummaryContext: 'PatientSummaryContext'
};


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent extends MessageBusAbstractComponent implements OnInit, OnDestroy {
  microfrontends: Microfrontend[] = [];
  appConfigs: any;
  patientSummaryLoaded = false;
  public psClientSendMessageBus = new PluginMessageBus('patient-summary-page');
  public psHostListenMessageBus = new HostMessageBus('patient-summary-host');

  constructor(
    private configService: ConfigService,
    protected appAuth: AppAuthService,
    protected patientService: PatientService,
    protected router: Router,
    protected ngZone: NgZone,
    private lookupService: LookupService,
  ) {
    super(appAuth, patientService, router, ngZone);
    this.appAuth.start();
    this._subscribeToPatientSummaryPageMessages();
    this.setHostMessageBus(new OdsHostMessageBus(OdsApplication.patientSummary));
  }

  async ngOnInit(): Promise<void> {
    this.microfrontends = await this.lookupService.lookup();
    const routes = buildRoutes(this.microfrontends);
    this.router.resetConfig(routes);
    const postMessageData: PostMessageData = {
      type: ParentPostMessageType.APP_INITIALIZED,
      value: OdsApplication.patientSummary,
    };
    // tslint:disable-next-line: tsr-detect-unsafe-cross-origin-communication
    console.log('host iframe message send: ', JSON.stringify(postMessageData, null, 2));
    window.parent.postMessage(postMessageData, '*');
  }

  public ngOnDestroy(): void {
    this.appAuth.end();
  }

  @HostListener('window:message', ['$event'])
  onMessageReceived(event: ReceiveMessageEvent) {
    console.log('host iframe message recieved: ', JSON.stringify(event.data, null, 2));
    switch(event.data.type) {
      case ParentReceiveMessageType.APP_CONFIGURATION:
        this.appConfigs = event.data.value;
        if (this.patientSummaryLoaded) {
          this._sendAppConfigsMessage(event.data.type);
        }
        break;
      case ParentReceiveMessageType.CAN_CLOSE_QUERY:
        const postMessageData: PostMessageData =  {
          type: ParentPostMessageType.CAN_CLOSE_RESULT,
          value: true,
        };
        // tslint:disable-next-line: tsr-detect-unsafe-cross-origin-communication
        console.log('host iframe message send: ', JSON.stringify(postMessageData, null, 2));
        window.parent.postMessage(postMessageData, '*');
        break;
    }
  }

  private _subscribeToPatientSummaryPageMessages(): void {
    this.psHostListenMessageBus.subscribe((message: IMessage) => {
      this.ngZone.run(() => this._onMessage(message));
    });
  }

  private _iSendMessage(message: IMessage): void {
    console.log('host messagebus message send: ', JSON.stringify(message, null, 2));
    this.psClientSendMessageBus.send(message);
  }

  private _sendAppConfigsMessage(recieveMessage) {
    this._iSendMessage({
      name: recieveMessage === messageCode.patientSummaryLoaded ? messageCode.patientSummaryContext :
        ParentReceiveMessageType.APP_CONFIGURATION,
      args: this.appConfigs,
    });
  }

  private _onMessage(message: IMessage): void {
    console.log('host messagebus message recieved: ', JSON.stringify(message, null, 2));
    switch (message.name) {
      case ParentPostMessageType.APP_INITIALIZED:
      case messageCode.patientSummaryLoaded:
        this.patientSummaryLoaded = true;
        if (this.appConfigs) {
          this._sendAppConfigsMessage(message.name);
        }
    }
  }
}



import { InjectionToken } from '@angular/core';

export const iconList: string[] = [
  'gear',
  'menu',
  'notifications',
  'search',
  'patients',
  'home',
  'tumor-board',
];
export const ICON_LIST: InjectionToken<string[]> = new InjectionToken<string[]>(
  'Custom icon list',
  {
    factory: () => iconList,
  },
);

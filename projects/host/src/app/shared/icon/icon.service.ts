import { Inject, Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

import { ICON_LIST } from './icon.const';

@Injectable()
export class IconService {
  private _assets_path: string = '../../assets/images/';

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    @Inject(ICON_LIST) private _iconList: string[],
  ) {}

  public init(): void {
    this._iconList.forEach((iconName: string) => {
      this._registerIcon(iconName);
    });
  }

  private _registerIcon(iconName: string): void {
    this.matIconRegistry.addSvgIcon(
      iconName,
      this.domSanitizer.bypassSecurityTrustResourceUrl(
        `${this._assets_path}icon-${iconName}.svg`,
      ),
    );
  }
}

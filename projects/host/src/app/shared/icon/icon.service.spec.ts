import { TestBed, waitForAsync } from '@angular/core/testing';

import { ICON_LIST } from './icon.const';
import { IconService } from './icon.service';

describe('IconService', () => {
  let service: IconService;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [IconService, { provide: ICON_LIST, useValue: [] }],
      });
    }),
  );

  beforeEach(() => {
    service = TestBed.inject(IconService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

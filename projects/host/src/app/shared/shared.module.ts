import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { IconService } from './icon/icon.service';

@NgModule({
  declarations: [],
  imports: [HttpClientModule],
  providers: [IconService],
})
export class SharedModule {}

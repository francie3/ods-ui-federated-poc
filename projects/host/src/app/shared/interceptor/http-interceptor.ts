import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import {AppAuthService} from '../../auth/app-auth.service';

@Injectable()
export class OdsHttpInterceptor implements HttpInterceptor {

  constructor(private appAuth: AppAuthService) {
  }

  public intercept(
    request: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
      const customRequest: HttpRequest<any> = request.clone({
        setHeaders: this.appAuth.getHeaders(),
      });
      return next.handle(customRequest);
  }

}

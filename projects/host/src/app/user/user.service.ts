import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from './user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private session$: BehaviorSubject<any> = new BehaviorSubject(null);

  public setSession(session: any): void {
    this.session$.next(session);
  }

  public getSession(): Observable<any> {
    return this.session$.asObservable();
  }

  public getUser(): Observable<User> {
    return this.session$.pipe(map((value) => value?.session?.profile));
  }
}

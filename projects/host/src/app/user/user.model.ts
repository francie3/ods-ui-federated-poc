export interface User {
  displayName: string;
  username: string;
}

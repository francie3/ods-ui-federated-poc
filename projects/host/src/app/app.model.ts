
export enum ParentReceiveMessageType {
    APP_CONFIGURATION = 'AppConfiguration',
    CAN_CLOSE_QUERY = 'CanCloseQuery',
}

export enum ParentPostMessageType {
    APP_INITIALIZED = 'AppInitialized',
    CAN_CLOSE_RESULT = 'CanCloseResult',
}

export enum OdsApplication {
    patientSummary = 'ods-patient-summary',
}

export interface PatientSummaryConfiguration {
    patientId: string;
    authToken: string;
    appContext: string;
    appOrigin: string;
    apiUrls: any;
    baseApiURL: string;
    tenantAlias: string;
    userClaims: any;
    roles: string;
    patientDetails: any;
    locale: string;
}

export interface ReceiveMessageData {
    type: ParentReceiveMessageType;
    value: PatientSummaryConfiguration | null;
}

export interface PostMessageData {
    type: ParentPostMessageType;
    value: OdsApplication | boolean;
}

export interface ReceiveMessageEvent {
    data: ReceiveMessageData;
}
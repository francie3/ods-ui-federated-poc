import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { PatientModel } from './patient.model';
import { PatientRemoteService } from './patient.remote.service';

@Injectable({
  providedIn: 'root',
})
export class PatientService {
  constructor(public patientRemoteService: PatientRemoteService) {}

  public getPatient(patientUuid: string): Observable<PatientModel> {
    return this.patientRemoteService.getPatient(patientUuid);
  }

}

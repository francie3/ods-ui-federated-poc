export interface PatientFacility {
  externalIdentifier: string;
  name: string;
  uuid: string;
  patientDisplayIdentifierSystem?: string;
  patientPrimaryIdentifierSystem?: string;
}

export interface PatientIdentifier {
  uuid: string;
  facility: string;
  system: string;
  value: string;
}


export interface PatientModel {
  uuid: string;
  firstName: string;
  lastName: string;
  fullName: string;
  abbreviation?: string;
  dateOfBirth: Date;
  facility: string;
  organizationUuid?: string;
  facilityDetails?: PatientFacility;
  gender: string;
  maritalStatus?: string;
  patientIdentifiers: PatientIdentifier[];
  serviceAccount?: boolean;
  versionId?: string;
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { PatientModel } from './patient.model';

@Injectable({
  providedIn: 'root',
})
export class PatientRemoteService {
  public baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {}

  public getPatient(patientUuid: string): Observable<PatientModel> {
    return this.http.get(
      `${this.baseUrl}/v1/patient/${patientUuid}`,
    ).pipe(map((response: any) => ({
      ...response,
      organizationUuid: response.facility
    }))) as Observable<PatientModel>;
  }

}

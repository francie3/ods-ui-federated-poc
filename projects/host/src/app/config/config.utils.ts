import { ensureProtocol } from '../common/url/url.utils';
import { AuthConfig, UiConfigRemote } from './config.model';

export const getTenantFromHost = (): string => {
  const { hostname } = window.location;
  const [tenantId] = hostname.split('.');

  return tenantId;
};

export const formAuthConfig = (uiConfig: UiConfigRemote): AuthConfig | null => {
  if (!uiConfig) {
    return null;
  }

  const tenantAlias: string = getTenantFromHost();

  return {
    provider: {
      type: 'okta',
      url: uiConfig.oktaUrl || '',
      clientId: uiConfig.oktaClientId || '',
    },
    platform: {
      url: ensureProtocol(uiConfig.apiGatewayUrl || ''),
    },
    app: {
      url: ensureProtocol(uiConfig.authUiUrl || ''),
    },
    client: {
      tenantAlias,
      appAlias: uiConfig.globalNavigationAppAlias,
    },
  };
};

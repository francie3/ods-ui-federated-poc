import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { UiConfigRemote, ConfigRemote, RemoteFrontends, REMOTE_TYPES } from './config.model';
import { ConfigRemoteService } from './config.remote.service';

@Injectable({
  providedIn: 'root',
})
export class ConfigService {
  constructor(public configRemoteService: ConfigRemoteService) {}

  public getUiAuthConfig(): Observable<UiConfigRemote> {
    return this.configRemoteService.getUiConfiguration();
  }

  public getConfig(): Observable<ConfigRemote> {
    return this.configRemoteService.getConfiguration();
  }

  public getRemoteFrontEnds(): Observable<RemoteFrontends> {
    return of(new Map([
      ['patientSummary',
        {
          remoteEntry: 'https://odsps.platform.navify.com:4300/remoteEntry.js',
          remoteName: 'patientsummary',
          exposedModule: './PatientFederatedModule',
          type: REMOTE_TYPES.module,
          routePath: 'patient/:patientUuid',
          className: 'PatientSummaryFederatedModule'
        }
      ],
      ['patientSummaryIFrame',
        {
          remoteEntry: 'https://odsps.platform.navify.com:4300',
          remoteName: '',
          exposedModule: '',
          type: REMOTE_TYPES.iFrame,
          routePath: 'iframe-patient/:patientUuid/module/:moduleId',
        }
      ],
      ['patientList',
        {
          remoteEntry: 'https://odsps.platform.navify.com:5000/remoteEntry.js',
          remoteName: 'ods_patient_list',
          exposedModule: './Component',
          type: REMOTE_TYPES.component,
          displayName: 'Patient List',
          className: 'PatientListComponent'
        }
      ]
    ]));
  }

}

import { LoadRemoteModuleOptions } from '@angular-architects/module-federation';

export interface UiConfigRemote {
  apiGatewayUrl: string;
  apiGatewayBasePath: string;
  authUiUrl: string;
  oktaUrl: string;
  oktaClientId: string;
  globalNavigationAppUuid: string;
  globalNavigationAppAlias: string;
}

export interface AuthConfig {
  provider: ProviderUiConfig;
  platform: PlatformUiConfig;
  app: AppUiConfig;
  client: ClientUiConfig;
}

export interface ProviderUiConfig {
  type: string;
  url: string;
  clientId: string;
}

export interface PlatformUiConfig {
  url: string;
}

export interface AppUiConfig {
  url: string;
}

export interface ClientUiConfig {
  appAlias: string;
  tenantAlias: string;
}

export interface AppRemote {
  orgUuid: string;
  url: string;
}

export enum ROLES {
  CUSTOMER_ADMIN = 'CUSTOMER_ADMIN',
}

export enum ACCESS {
  ALL = 'ALL',
}

export enum UserPermissions {
  UserEdit = 'USER_CRUD',
  UserTask = 'USER_TASK',
  PatientRead = 'PATIENT_DATA_R',
  PatientEdit = 'PATIENT_DATA_CRUD',
  PatientDelete = 'PATIENT_DELETE',
  PatientExport = 'PATIENT_EXPORT',
  PatientAnonymization = 'PATIENT_ANONYMIZATION',
  PatientApps = 'PATIENT_APPS',
  PatientTimelineEdit = 'PATIENT_TIMELINE_CRUD',
  PatientTimelineRead = 'PATIENT_TIMELINE_R',
  SendToEmr = 'SEND_TO_EMR',
  MeetingEdit = 'MEETING_CRUD',
  MeetingRead = 'MEETING_R',
  MeetingPin = 'MEETING_PIN',
  MeetingRecommendationsEdit = 'NEXT_STEP_CRUD',
}

export interface FeatureFlagConfig {
  featureName: string;
  flag: string;
}

export interface ConfigRemote {
  isCupStudy: boolean;
  apps: AppRemote[];
  access: ACCESS;
  email: string;
  eulaVersion: number;
  facilityID: string;
  firstName: string;
  isServiceAccount: boolean;
  featureFlagsEnabled: boolean;
  featureFlags: FeatureFlagConfig[];
  odsPatientSummaryUiUrl: string;
  odsPatientSummaryApiUrl: string;
  lastName: string;
  oktaID: string;
  releaseNotesVersion: number;
  role: ROLES;
  sessionTimeoutIntervalInMinutes: number;
  thumbnail: string;
  permissions: UserPermissions[];
}

export enum REMOTE_TYPES {
  iFrame =  'IFRAME',
  module = 'MODULE',
  component = 'COMPONENT',
  webComponent = 'WEB_COMPONENT',
}

export interface RemoteFrontend extends LoadRemoteModuleOptions {
  type: REMOTE_TYPES;
  routePath?: string;
  displayName?: string;
  className?: string;
}

export type RemoteFrontends = Map<string, RemoteFrontend>;

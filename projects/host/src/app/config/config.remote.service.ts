import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { UiConfigRemote, ConfigRemote } from './config.model';


@Injectable({
  providedIn: 'root',
})
export class ConfigRemoteService {
  public baseUrl: string = environment.baseUrl;

  constructor(private http: HttpClient) {}

  public getUiConfiguration(): Observable<UiConfigRemote> {
    return this.http.get(
      `${this.baseUrl}/v1/ui-configuration`,
    ).pipe(
      map((response: any) => ({
        ...response.platform,
        apiGatewayUrl: response.platform.apiGatewayURL,
        globalNavigationAppAlias: response.platform.tumorboardAppAlias
      }))
    ) as Observable<UiConfigRemote>;
  }

  public getConfiguration(): Observable<ConfigRemote> {
    return this.http.get(
      `${this.baseUrl}/v1/configuration`,
    ) as Observable<ConfigRemote>;
  }

}

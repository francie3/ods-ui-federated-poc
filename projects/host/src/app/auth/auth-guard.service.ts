import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';

import { UserService } from '../user/user.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private userService: UserService, private router: Router) {}
  public canActivate(): Observable<boolean> {
    const userSession$: Observable<any> = this.userService.getSession();

    return userSession$.pipe(
      filter((session: any) => Boolean(session)),
      take(1),
      map((session: any) => !session.error),
    );
  }
}

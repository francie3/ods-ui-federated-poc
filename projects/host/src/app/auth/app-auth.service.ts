import { Location } from '@angular/common';
import { Injectable, NgZone } from '@angular/core';
import {
  Auth,
  AUTH_EVENT_AFTER_LOGIN,
  AUTH_EVENT_SESSION_EXPIRATION,
  AuthEvent,
  AuthOptions,
  getUrlDetails,
  AuthLoginIframeArgs,
  AuthLoginReason,
} from '@dia/auth';
import { combineLatest, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { AuthConfig, ConfigRemote, UiConfigRemote } from '../config/config.model';
import { ConfigService } from '../config/config.service';
import { formAuthConfig, getTenantFromHost} from '../config/config.utils';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root',
})
export class AppAuthService {
  private auth!: Auth;
  private authConfig: AuthConfig | null = null;
  private config: ConfigRemote | null = null;
  private tenantAlias: string = getTenantFromHost();
  private organizationUuid: string = '';
  private patientUuid: string = '';
  private end$: Subject<void> = new Subject<void>();

  constructor(
    private configService: ConfigService,
    private userService: UserService,
    private location: Location,
    public ngZone: NgZone,
  ) {}

  public start(): void {
    this.configService
      .getUiAuthConfig()
      .pipe(
        map((value: UiConfigRemote) => formAuthConfig(value)),
        takeUntil(this.end$),
      )
      .subscribe((value: AuthConfig | null) => {
        this.authConfig = value;
        this.init(value);
      });
  }

  public init(config: any): void {
    const authOptions: AuthOptions = this.getAuthOptions();
    this.auth = new Auth(config, authOptions);

    this.auth.subscribe((event: AuthEvent) => {
      switch (event.type) {
        case AUTH_EVENT_AFTER_LOGIN:
          this.checkSession();
          break;
        case AUTH_EVENT_SESSION_EXPIRATION:
          this.logout();
          break;
        default:
          break;
      }
    });

    this.auth.init().then(() => {
      this.checkSession();
    });
  }

  public end(): void {
    this.end$.next();
    this.end$.complete();
  }

  private doLogin(): void {
    if ((document as any).documentMode || /Edge|MSIE/.test(navigator.userAgent)) {
      this.auth.loginRedirect({
        returnTo: window.location.href,
        state: null,
      });
    } else {
      this.ngZone.runOutsideAngular(() => {
        this.auth.loginIframe(this.authConfig);
      });
    }
  }

  public logout(): void {
    this.auth.logout().then(() => {
      this.location.go('/');
      document.location.reload();
    });
  }

  public getConfig(): ConfigRemote | null {
    return this.config;
  }

  public setOrganizationUuid(organizationUuid: string) {
    this.organizationUuid = organizationUuid;
  }

  public setPatientUuid(patientUuid: string) {
    this.patientUuid = patientUuid;
  }

  public getHeaders(): {[name: string]: string | string[]} {
    return {
      ['X-Navify-Tenant']: this.tenantAlias,
      ['X-Navify-PatientId']: this.patientUuid,
      ['dis-fhir-organization']: this.organizationUuid,
    };
  }


  private loadConfig(): void {
    this.configService
      .getConfig()
      .pipe(
        takeUntil(this.end$),
      )
      .subscribe((value: ConfigRemote | null) => {
        this.config = value;
      });
  }

  private checkSession(loggedOut = false): void {
    combineLatest([this.auth.getSession(), this.auth.getLoginReturn()])
      .pipe(takeUntil(this.end$))
      .subscribe(
        ([session, loginResult]) => {
          if ((session || loginResult) && !loggedOut) {
            this.userService.setSession({ session, loginResult, error: null });
            this.loadConfig();
          } else {
            this.doLogin();
            /* this.auth.loginRedirect({
              returnTo: window.location.origin,
              state: null,
            }); */
          }
        },
        (err) => {
          this.doLogin();
          /* this.auth.loginRedirect({
            returnTo: window.location.origin,
            state: null,
          }); */
          throw err;
        },
      );
  }

  private getAuthOptions(): AuthOptions {
    const { href: rocheLogoImageSrc } = getUrlDetails(
      'assets/images/roche-logo-blue.svg',
    );
    const { href: gehcLogoImageSrc } = getUrlDetails(
      'assets/images/gehc-logo-blue.svg',
    );

    const authOptions: AuthOptions = {
      redirectUrlUseHash: true,
      customPageTitle: 'NAVIFY® Tumor Board',
      customHeadline: '<strong>NAVIFY<sup>®</sup></strong> Oncology',
      customFooter: `<div class="right">
            <a href="https://roche.com" target="_blank"><img src="${rocheLogoImageSrc}" class="big" /></a>
            <span class="separator big"></span>
            <img src="${gehcLogoImageSrc}" class="big" />
            </div>`,
    };
    return authOptions;
  }

}

const URL_SCHEMA_CHECK_REGEXP: RegExp = /^[a-z]+:\/\//;
const PATHNAME_TRIM_REGEXP: RegExp = /^[/]*|[/]*$/g;

export const ensureProtocol = (url: string): string => {
  if (URL_SCHEMA_CHECK_REGEXP.test(url)) {
    return url;
  }

  const { protocol }: Location = window.location;
  return `${protocol}//${url}`;
};

export const fixPathname = (pathname: string): string => {
  return `/${pathname.replace(PATHNAME_TRIM_REGEXP, '')}`;
};

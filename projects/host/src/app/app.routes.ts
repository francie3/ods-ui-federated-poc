import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ConfigComponent } from './config/config.component';
import { environment } from '../environments/environment';

export const APP_ROUTES: Routes = [
    {
      path: '',
      redirectTo: `patientid/${environment.defaultPatientId}`,
      pathMatch: 'full'
    },
    {
      path: 'patientid/:patientUuid',
      component: HomeComponent,
      pathMatch: 'full'
    },
    {
      path: 'config',
      component: ConfigComponent
    }
];

import { UserPermissions } from '../config/config.model';
import { PatientModel } from '../patient/patient.model';

export enum OdsApplication {
  patientSummary = 'ods-patient-summary',
}

export enum PostMessageType {
  APP_CONFIGURATION = 'AppConfiguration',
  CAN_CLOSE_QUERY = 'CanCloseQuery',
}

export enum ReceiveMessageType {
  APP_INITIALIZED = 'AppInitialized',
  CAN_CLOSE_RESULT = 'CanCloseResult',
}

export interface ReceiveMessageData {
  type: ReceiveMessageType;
  value: OdsApplication | boolean;
}

export interface ReceiveMessageEvent {
  data: ReceiveMessageData;
}

export interface PatientSummaryConfiguration {
  patientId: string;
  authToken: string | null;
  appContext: string | null;
  appOrigin: string | null;
  apiUrls: string | null;
  baseApiURL: string;
  tenantAlias: string;
  userClaims: UserPermissions[];
  roles: string;
  patientDetails: PatientModel | null;
  locale: string | null;
}

export interface PostMessageData {
  type: PostMessageType;
  value: PatientSummaryConfiguration | null;
}



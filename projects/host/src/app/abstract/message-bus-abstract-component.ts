import { Component, NgZone, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivationEnd } from '@angular/router';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { HostMessageBus, IMessage } from '@ods/message-bus';

import { PatientModel } from '../patient/patient.model';
import { PatientService } from '../patient/patient.service';
import { ReceiveMessageType, PostMessageType, PatientSummaryConfiguration } from './message.model';
import { ConfigRemote } from '../config/config.model';
import { AppAuthService } from '../auth/app-auth.service';
import { getTenantFromHost } from '../config/config.utils';

@Component({
  template: ''
})
export abstract class MessageBusAbstractComponent implements OnInit, OnDestroy {

  private psHostMessageBus: HostMessageBus | null = null;
  private psInitialized = false;
  private patientDetails: PatientModel | null = null;
  private tenantAlias = getTenantFromHost();

  protected end$: Subject<void> = new Subject<void>();

  protected constructor(
    protected appAuth: AppAuthService,
    protected patientService: PatientService,
    protected router: Router,
    protected ngZone: NgZone,
  ) {
    this.subscribeRouteChange();
  }

  public ngOnInit() {
  }

  public ngOnDestroy() {
    this.end$.next();
    this.end$.complete();
  }

  protected setHostMessageBus(messageBus: HostMessageBus) {
    this.psHostMessageBus = messageBus;
    this.subscribeToPatientSummaryPageMessages();
  }


  private getPatientDetails(patientId: string | null): void {
    if (patientId) {
      this.patientService.getPatient(patientId).pipe(take(1)).subscribe((patient: PatientModel) => {
        this.patientDetails = patient;
        this.appAuth.setOrganizationUuid(patient.organizationUuid!);
        this.appAuth.setPatientUuid(patient.uuid);
        if (patient && this.psInitialized) {
          this.sendAppConfigsMessage();
        }
      });
    }
  }

  private subscribeRouteChange(): void {
    this.router.events.pipe(takeUntil(this.end$)).subscribe(e => {
      if (e instanceof ActivationEnd) {
        const patientId = e.snapshot.paramMap.get('patientUuid');
        this.getPatientDetails(patientId);
      }
    });
  }

  private subscribeToPatientSummaryPageMessages(): void {
    this.psHostMessageBus!.subscribe((message: IMessage) => {
      this.ngZone.run(() => this.onMessage(message));
    });
  }

  private _sendMessage(message: IMessage): void {
    this.psHostMessageBus!.sendToClient(message);
  }

  private getPatientConfiguraton(): PatientSummaryConfiguration {
    const config: ConfigRemote | null = this.appAuth.getConfig();

    return {
      patientId: this.patientDetails ? this.patientDetails.uuid : '',
      authToken: null,
      appContext: 'TB',
      appOrigin: null,
      apiUrls: null,
      baseApiURL: config ? config.odsPatientSummaryApiUrl : '',
      tenantAlias: this.tenantAlias,
      userClaims: config ? config.permissions : [],
      roles: config ? config.role : '',
      patientDetails: this.patientDetails ? this.patientDetails : null,
      locale: null, // this._locale,
    };
  }

  private sendAppConfigsMessage(): void {
    const patientConfig: PatientSummaryConfiguration = this.getPatientConfiguraton();

    this._sendMessage({
      name: PostMessageType.APP_CONFIGURATION,
      args: patientConfig,
    });
  }

  private onMessage(message: IMessage): void {
    switch (message.name) {
      case ReceiveMessageType.APP_INITIALIZED:
        this.psInitialized = true;
        if (this.patientDetails) {
          this.sendAppConfigsMessage();
        }
    }
  }

}

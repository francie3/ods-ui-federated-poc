const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const mf = require("@angular-architects/module-federation/webpack");
const path = require("path");

const sharedMappings = new mf.SharedMappings();
sharedMappings.register(
  path.join(__dirname, '../../tsconfig.json'),
  [/* mapped paths to share */
  ]);
module.exports = {
  output: {
    uniqueName: "ods-federated-host"
  },
  optimization: {
    // Only needed to bypass a temporary bug
    runtimeChunk: false
  },
  plugins: [
    new ModuleFederationPlugin({
      remotes: {
      },
      shared: {
        "@angular/core": { singleton: true, strictVersion: false },
        "@angular/common": { singleton: true, strictVersion: false },
        "@angular/common/http": { singleton: true, strictVersion: false },
        "@angular/router": { singleton: true, strictVersion: false },
        "@angular/platform-browser": { singleton: true, strictVersion: false },
        "@angular/platform-browser/animations": { singleton: true,  strictVersion: false},
        "@ngrx/store": { singleton: true, strictVersion: false },
        "@ngrx/effects": { singleton: true, strictVersion: false },
        "@ngrx/router-store": { singleton: true, strictVersion: false },
        "@rxjs": { singleton: true, strictVersion: false },
        "@angular/material": { singleton: true, strictVersion: false },
        "@angular/material/snack-bar": { singleton: true, strictVersion: false },
        '@angular/material/dialog': { singleton: true, strictVersion: false },
        ...sharedMappings.getDescriptors()
      },
    }),
  ],
};

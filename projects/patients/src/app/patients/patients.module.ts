import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientsSearchComponent } from './patients-search/patients-search.component';
import { RouterModule } from '@angular/router';
import { PATIENTS_ROUTES } from './patients.routes';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PATIENTS_ROUTES)
  ],
  declarations: [
    PatientsSearchComponent
  ]
})
export class PatientsModule { }

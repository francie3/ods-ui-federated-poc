import { Routes } from '@angular/router';
import { PatientsSearchComponent } from './patients-search/patients-search.component';

export const PATIENTS_ROUTES: Routes = [
    {
      path: '',
      redirectTo: 'patients-search'
    },
    {
      path: 'patients-search',
      component: PatientsSearchComponent
    }
];
